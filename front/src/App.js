import React, { useEffect, useState } from 'react';
import NewsRouter from './router/NewsRouter';
import HomeRouter from './router/HomeRouter';
import DashboardRouter from './router/DashboardRouter';
import { Navigate, Outlet } from 'react-router-dom';

import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  useParams,
  useRouteMatch
} from "react-router-dom";
import Login from './components/Login/Login';
import WorkingMode from './components/WorkingMode/WorkingMode';
import WorkingModeRouter from './router/NavbarRouter';
import { session } from './API/AuthApi';
import InfoRouter from './router/InfoRouter';

const App = (props) => {

  const [lang, setLanguage] = useState(0)

  useEffect(() => {
    let html = document.getElementById('rootHTML')
    let color = parseInt(localStorage.getItem('isLight'))
    color ? html.setAttribute('data-theme', 'light') : html.setAttribute('data-theme', 'dark')
  }, [])

  useEffect(() => {
    setLanguage(lang)
  }, [lang])

  const AuthRoute = ({ children }) => {
    const checkAuth = async () => {
      let token = localStorage.getItem('token')
      const isAuth = await session(token)
      return isAuth
    }
    return checkAuth() && children
  }
  return (
    <>
      <Router>
        <>
          <Routes>
            <Route path="/dashboard" element={
              <AuthRoute>
                <DashboardRouter />
              </AuthRoute>
            }></Route>
            <Route exact path="/" element={<HomeRouter setLanguage={setLanguage} />}></Route>
            <Route exact path="/info" element={<InfoRouter />}></Route>
            <Route exact path="/news" element={<NewsRouter />}></Route>
            <Route exact path="/login" element={<Login />}></Route>
            <Route exact path="/working-mode" element={<WorkingModeRouter />}></Route>
          </Routes>
        </>
      </Router>
    </>
  );
};
export default App;
