import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './app.css';
import './responsive.css';

ReactDOM.render(<App />, document.getElementById('root'));
