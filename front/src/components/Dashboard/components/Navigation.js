import React, { useState } from 'react'

import NewspaperIcon from '@mui/icons-material/Newspaper';
import SegmentIcon from '@mui/icons-material/Segment';
import AddchartIcon from '@mui/icons-material/Addchart';

export default function Navigation({ onChange }) {

    const [isActive, setIsActive] = useState('segment')

    const onChangeActive = (status) => {
        setIsActive(status)
        onChange(status)
    }

    return (
        <div className='navigation'>
            <button onClick={() => onChangeActive('segment')} className={isActive === 'segment' ? 'active' : ''}>
                <SegmentIcon />
            </button>
            <button onClick={() => onChangeActive('newspaper')} className={isActive === 'newspaper' ? 'active' : ''}>
                <NewspaperIcon />
            </button>
            {/* <button onClick={() => onChangeActive('addchart')} className={isActive === 'addchart' ? 'active' : ''}>
                <AddchartIcon />
            </button> */}
        </div>
    )
}