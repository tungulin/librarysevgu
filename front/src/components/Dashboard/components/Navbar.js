import React, { useEffect, useState } from 'react'
import sevguLogo from '../../../images/sevgu-logo.svg'
import { useNavigate } from "react-router-dom";

export default function Navbar() {

    const [user, setUser] = useState({})
    let navigate = useNavigate();

    useEffect(() => {
        let user = JSON.parse(localStorage.getItem('user'))
        setUser(user)
    }, [])

    const stopSession = () => {
        localStorage.clear('user')
        localStorage.clear('token')
        navigate("/");
    }

    return (
        <div className='dashboard-navbar'>
            <div className='navbar__left'>
                <img className='navbar__left__logo' src={sevguLogo} />
                <div className='navbar__left__title'>Dashboard</div>
            </div>
            <div className='navbar__center'></div>
            <div className='navbar__right'>
                <div className='navbar__right__name'>Hello, {user.name} !</div>
                <button class="btn" onClick={stopSession}>Logout</button>
            </div>
        </div >
    )
}
