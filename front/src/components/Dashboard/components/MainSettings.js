import React from 'react'
import { useForm } from "react-hook-form";
import { createInfo } from '../../../API/AuthApi';

export default function MainSettings() {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const createInfoData = async (data) => {
        let user = JSON.parse(localStorage.getItem('user'))
        await createInfo({ ...data, token: user.token })
    }

    const createNewsData = async (data) => {
        // let user = JSON.parse(localStorage.getItem('user'))
        // await createInfo({ ...data, token: user.token })
    }


    return (
        <div>
            <div className='dashboard__body'>
                <form onSubmit={handleSubmit(createInfoData)}>
                    <div className="card-title"> Информация</div>
                    <div className="card mr-5 w-96 h bg-neutral text-neutral-content">
                        <div className="card-body items-center text-center">
                            <div className='login__modal__inputs'>
                                <div class="form-control w-full ">
                                    <label class="label">
                                        <span class="label-text">Title</span>
                                    </label>
                                    <input type="text" placeholder="Введите свой логин" {...register("title")} class="input input-bordered w-full" />
                                </div>
                                <div class="form-control w-full ">
                                    <label class="label">
                                        <span class="label-text">Info</span>
                                    </label>
                                    <input type="text" placeholder="Введите свой пароль" {...register("info")} class="input input-bordered w-full" />
                                </div>
                                <div className='btns__group'>
                                    <button class="btn">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}
