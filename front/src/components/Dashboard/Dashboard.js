import React, { useState } from 'react'
import AddChart from './components/AddChart'
import MainSettings from './components/MainSettings'
import Navbar from './components/Navbar'
import Navigation from './components/Navigation'
import NewsAdd from './components/NewsAdd'
import './Dashboard.css'

export default function Dashboard() {

    const [navStatus, setNavStatus] = useState('segment')

    return (
        <div className='dashboard'>
            <Navbar />
            <Navigation onChange={value => setNavStatus(value)} />

            {navStatus === 'segment' && <div className='dashboard__body'>
                <MainSettings />
            </div>}
            {navStatus === 'newspaper' && <div className='dashboard__body'>
                <NewsAdd />
            </div>}
            {/* {navStatus === 'addchart' && <div className='dashboard__body'>
                <AddChart />
            </div>} */}
        </div>
    )
}
