
const NoNavbar = (props) => {

  return (
    <div className="no-burger navbarTheme navbar-center">
      <ul className="menu menu-horizontal p-0">
        <li>
          <a>О Библиотеке</a>
          <ul style={{ position: 'absolute', zIndex: '100000' }} class="menu menu-compact shadow-lg bg-base-200 p-2 w-60 rounded-box">
            <li className='hover-bordered'><a>Общая информация</a></li>
            <li className="hover-bordered"><a>Структура</a></li>
            <li className='hover-bordered'><a>Режим работы</a></li>
            <li className='hover-bordered'><a>Правила пользования</a></li>
            <li className='hover-bordered'><a href='/news' >Новости</a></li>
          </ul>
        </li>
        <li><a>Ресурсы</a></li>
        <li>
          <a>Электронные ресурсы </a>
          <ul style={{ position: 'absolute', zIndex: '100000' }} class="menu menu-compact shadow-lg bg-base-200 p-2 w-60 rounded-box">
            <li className='hover-bordered'><a>Российские: Киберленинка</a></li>
            <li className="hover-bordered"><a>Зарубежные</a></li>
            <li className="hover-bordered"><a>Электронный каталог</a></li>
            <li className="hover-bordered"><a>Электронный репозиторий</a></li>
          </ul>
        </li>
        <li>
          <a>Студенту </a>
          <ul style={{ position: 'absolute', zIndex: '100000' }} class="menu menu-compact shadow-lg bg-base-200 p-2 w-60 rounded-box">
            <li className='hover-bordered'><a>Правила пользования</a></li>
            <li className="hover-bordered"><a>Работа с библиотекой </a></li>
            <li className="hover-bordered"><a>ВКР</a></li>
          </ul>
        </li>
        <li>
          <a>Преподавателю</a>
          <ul style={{ position: 'absolute', zIndex: '100000' }} class="menu menu-compact shadow-lg bg-base-200 p-2 w-60 rounded-box">
            <li className='hover-bordered'><a>Заказ литературы</a></li>
            <li className="hover-bordered"><a>Индексирование УДК И ББК</a></li>
            <li className="hover-bordered"><a>Разработчику РПД</a></li>
            <li className="hover-bordered"><a>Выгрузка ВКР</a></li>
            <li className="hover-bordered"><a>Автору</a></li>
          </ul>
        </li>
        <li>
          <a href='/info'>Информация!</a>
        </li>
      </ul>

    </div>
  )
}

export default NoNavbar