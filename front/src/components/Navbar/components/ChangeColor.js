import OpacityIcon from '@mui/icons-material/Opacity';
import { useEffect, useState } from 'react';

// import "../Navbar.css"

const ChangeColor = (props) => {
  const [color,setColor] = useState(1)

  useEffect(()=>{
    let color = parseInt(localStorage.getItem('isLight'))

    setColor(color)
  },[])

  useEffect(()=>{
    let html = document.getElementById('rootHTML')
    color ? html.setAttribute('data-theme','light') : html.setAttribute('data-theme','dark')
  },[color])
    
  return (
      <div className="changeColor dropdown  mr-3">
      <label tabIndex="0" className="btn btn-ghost btn-circle avatar">
        <OpacityIcon/>
      </label>
      <ul tabIndex="0" className=" menu dropdown-content shadow rounded-box p-2 bg-base-200">
        <li onClick={()=>{
          localStorage.setItem('isLight','1')
          setColor(1)
          }}>
          <a>
            <div className={`border-base-content/20 ${color ? 'outline':''} wrapped light`}  ></div>
          </a>
        </li>
        <li onClick={()=>{
          localStorage.setItem('isLight','0')
          setColor(0)
          }}>
          <a>
            <div className={`border-base-content/20 ${!color ? 'outline':''} wrapped black`}></div>
          </a>
        </li>
      </ul>
    </div>
  )
}

export default ChangeColor