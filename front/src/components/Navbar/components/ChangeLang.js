import OpacityIcon from '@mui/icons-material/Opacity';
import { useEffect, useState } from 'react';
import { HomeLocalization, NavbarLocalization } from '../../core/localization';

const ChangeLang = ({ setLanguage }) => {
  const [lang, setLang] = useState('rus')

  return (
    <div className="changeColor dropdown  mr-3">
      <label tabIndex="0" className="btn btn-ghost btn-circle avatar">
        {lang === 'rus' ? "RUS" : "US"}
      </label>
      <ul tabIndex="0" className=" menu dropdown-content shadow rounded-box p-2 bg-base-200">
        <li>
          <a onClick={() => setLang('rus')}>
            RUS
          </a>
        </li>
        <li>
          <a onClick={() => setLang('eng')}>
            US
          </a>
        </li>
      </ul>
    </div>
  )
}

export default ChangeLang