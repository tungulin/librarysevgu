import { slide as Menu } from 'react-burger-menu';
import MenuIcon from '@mui/icons-material/Menu';

const Burger = (props) => {


  return (
    <div className="burger navbarTheme navbar-right">
      <Menu right pageWrapId={"page-wrap"} customBurgerIcon={<MenuIcon />}>
        <a>О Библиотеке</a>
        <a>Ресурсы</a>
        <a>Электронные ресурсы </a>
        <a>Студенту </a>
        <a>Преподавателю</a>
      </Menu>
    </div>
  )
}

export default Burger