import React, { useState } from 'react';


/*SVG*/
import sevguLogo from '../../images/sevgu-logo.svg'
import backgroundJPG from '../../images/backgroundNavbar.jpeg'
import { useEffect } from 'react';

import ChangeColor from './components/ChangeColor'
import ChangeLang from './components/ChangeLang';

import AccessTimeIcon from '@mui/icons-material/AccessTime';
import TelegramIcon from '@mui/icons-material/Telegram';
import VisibilityIcon from '@mui/icons-material/Visibility';

import Burger from './components/Burger';
import NoNavbar from './components/NoNavbar';

import "./Navbar.css"

const Navbar = (props) => {

  const [sticky, setSticky] = useState(false)
  const [user, setUser] = useState({})

  useEffect(() => {
    window.onscroll = () => { stickyScroll() }
  }, [])

  useEffect(() => {
    let userInfo = JSON.parse(localStorage.getItem('user'))
    setUser(userInfo)
  }, [localStorage.getItem('user')])

  const stickyScroll = () => {
    // Get the navbar
    var navbar__data = document.getElementById("navbar__data");

    // Get the offset position of the navbar
    var sticky = navbar__data.offsetHeight;

    if (window.pageYOffset >= sticky) {
      setSticky(true)
    } else {
      setSticky(false)
    }
  }

  const stopSession = () => {
    localStorage.clear('user')
    localStorage.clear('token')
    window.location. reload()
  }

  return (
    <div className="navbar bg-base-100">
      <div id='navbar__data' className={`navbar__data ${sticky ? 'sticky_wrap' : ''}`}>
        <div className='navbar__data_img'><img src={backgroundJPG} /></div>
        <div className='navbar__data__left'>
          <img src={sevguLogo} />
          <div className='title'>БИБЛИОТЕКА СЕВАСТОПОЛЬСКОГО ГОСУДАРСТВЕННОГО УНИВЕРСИТЕТА</div>
        </div>
        <div className='navbar__data__right'>
          <a className='dataBtn' href='/working-mode'>
            <div className='dataBtn__svg'><AccessTimeIcon /></div>
            <div className='dataBtn__title'>Режим работы</div>
          </a>
          <div className='dataBtn'>
            <div className='dataBtn__svg'><TelegramIcon /></div>
            <div className='dataBtn__title'>Мы в телеграме</div>
          </div>
          <div className='dataBtn'>
            <div className='dataBtn__svg'><VisibilityIcon /></div>
            <div className='dataBtn__title'>Слабовидящим</div>
          </div>
        </div>
      </div>
      <div id='navbar' className={`navbar__info bg-base-200 ${sticky ? 'sticky  ' : ''}`}>
        <Burger />
        <NoNavbar />

        <div className="navbar-end">
          <ChangeColor />
          <ChangeLang setLanguage={props.setLanguage} />
          <div className="dropdown dropdown-end">
            <label tabIndex="0" className="btn btn-ghost btn-circle avatar">
              <div className="avatar w-10 bg-neutral rounded-full">
                {!user && <div className=''>L</div>}
                {user && <div className=''>{user.name?.slice(0, 1)}</div>}
              </div>
            </label>
            <ul style={{ position: 'absolute', zIndex: '100000' }} tabIndex="0" className="mt-3 p-2 shadow-md menu menu-compact dropdown-content bg-base-200 rounded-box w-60">
              {!user && <li><a href='/login'>Login</a></li>}
              {user && <li><a>{user.name}</a></li>}
              {user && <li><a onClick={stopSession}>Logout</a></li>}
            </ul>
          </div>
        </div>
      </div>

    </div>
  );
};
export default Navbar;
