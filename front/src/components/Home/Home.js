import React from 'react';
import Breadcust from '../Common/Breadcust';
import { HomeLocalization } from '../core/localization';
import './Home.css'

const Home = (props) => {
  return (
    <div className='home'>
      <Breadcust breadcustInfo={['О библиотеке', 'Общая информация']} />
      <div className='wrappped'>
        <div className='home-left'>
          <div className="carousel w-full">
            <div id="slide1" className="carousel-item relative w-full">
              <img src={require("../../images/sevgu.png")} className="w-full" />
              <div class="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 bottom-0">
              </div>
            </div>
            <div id="slide2" className="carousel-item relative w-full">
              <img src={require("../../images/sevgu2.jpeg")} className="w-full" />
              <div class="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 bottom-0">
              </div>
            </div>
          </div>
          <div class="grid grid-flow-col mt-7 gap-5 text-center auto-cols-max date">
            <div class="flex flex-col p-2 bg-neutral rounded-box text-neutral-content date-content">
              <span class="countdown font-mono text-5xl">
                <span className="--value:10;"></span>
              </span>
              hours
            </div>
            <div class="flex flex-col p-2 bg-neutral rounded-box text-neutral-content date-content">
              <span class="countdown font-mono text-5xl">
                <span className="--value:10;"></span>
              </span>
              min
            </div>
            <div class="flex flex-col p-2 bg-neutral rounded-box text-neutral-content date-content">
              <span class="countdown font-mono text-5xl">
                <span className="--value:53;"></span>
              </span>
              sec
            </div>
          </div>
        </div>
        <div className='home-right'>
          <div className='home-right__title'>{HomeLocalization.title}</div>
          <div className='home-right__info'> {HomeLocalization.homeRightInfo1}</div>
          <div className='home-right__info'>{HomeLocalization.homeRightInfo2}</div>

        </div>
      </div>
    </div>
  );
};
export default Home;