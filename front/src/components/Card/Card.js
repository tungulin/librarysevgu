import { useEffect, useState } from 'react';
import Modal from 'react-modal';
import { useForm } from "react-hook-form";

import "./Card.css"
import { createComment, getComments } from '../../API/AuthApi';

const Card = (props) => {
    const [modalIsOpen, setIsOpen] = useState(false);
    const [commentsData, setComments] = useState([]);
    const { register, handleSubmit, formState: { errors } } = useForm();

    useEffect(() => {
        Modal.setAppElement(document.getElementById('root'));
    }, [])

    useEffect(() => {
        if (modalIsOpen) {
            getDataApi()
        }
    }, [modalIsOpen])

    const openModal = () => {
        setIsOpen(true);
    }

    const closeModal = () => {
        setIsOpen(false);
    }

    const getDataApi = async () => {
        let comments = await getComments(props.id)
        setComments([...comments.items])
    }

    const sendComment = async (data) => {
        let comments = await createComment({ ...data, id: props.id })
        setComments([...comments])
    }


    return (
        <div style={props.style} class="card lg:card-side bg-accent shadow-xl" onClick={openModal}>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                contentLabel="Example Modal"
                className="Modal"
                overlayClassName="Overlay"
            >
                <div className='leftModal'>
                    <img style={{ height: '100%', minWidth: "300px" }} src={props.img} />
                </div>
                <div className='rightModal'>
                    <form onSubmit={handleSubmit(sendComment)}>

                        <div className='title'>{props.title}</div>
                        <div className='subtitle'>{props.subtitle}</div>
                        <div className='rightModal__comments'>
                            {console.log('commentsData', commentsData)}
                            {commentsData.map(item => {
                                return <div className='comment'>
                                    <div className='comment__logo'>{item.name?.slice(0, 1)}</div>
                                    <div className='comment__comment'>{item.info}</div>
                                </div>
                            })}
                        </div>
                        <div className='formInput'>
                            <input type="text" placeholder='Оставьте ваш комментарий...' {...register("info")} />
                            <button className='btn btn-sm'>Submit</button>
                        </div>
                        <div className='postilkda'></div>
                    </form>
                </div>

            </Modal>
            <figure><img style={{ height: '100%', width: "200px" }} src={props.img} alt="Album" /></figure>
            <div class="card-body">
                <h2 class="card-title">{props.title}</h2>
                <p>{props.subtitle.slice(0, 120)}...</p>
            </div>
        </div >
    )
}

export default Card