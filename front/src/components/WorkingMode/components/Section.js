import "../WorkingMode.css"

import LocationOnIcon from '@mui/icons-material/LocationOn';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';

const Section = (props) => {

  return (
    <>
      <div className="workingMode__section">
        <div className="title">Севастополь, ул. Университетская, 29</div>
        <div className="info">
          <div className="infoSection">
            <div className="infoSection__svg"><LocationOnIcon /></div>
            <div className="infoSection__title">Севастополь, ул. Университетская, 29, каб ...</div>
          </div>
        </div>
        <div className="info">
          <div className="infoSection">
            <div className="infoSection__svg"><LocalPhoneIcon /></div>
            <div className="infoSection__title">+7 978 000 00</div>
          </div>
        </div>
        <div className="info">
          <div className="infoSection">
            <div className="infoSection__svg"><AccessTimeIcon /></div>
            <div className="infoSection__title">ПН-ЧТ 9:00–18:00</div>
          </div>
        </div>
        <div className="info">
          <div className="infoSection">
            <div className="infoSection__svg"><CalendarMonthIcon /></div>
            <div className="infoSection__title">Последняя пятница санитарный день</div>
          </div>
        </div>
      </div>

      <div className="workingMode__section">
        <div className="title">Севастополь, ул. Курчатова, 7</div>
        <div className="info">
          <div className="infoSection">
            <div className="infoSection__svg"><LocationOnIcon /></div>
            <div className="infoSection__title">Севастополь, ул. Университетская, 29, каб ...</div>
          </div>
        </div>
        <div className="info">
          <div className="infoSection">
            <div className="infoSection__svg"><LocalPhoneIcon /></div>
            <div className="infoSection__title">+7 978 000 00</div>
          </div>
        </div>
        <div className="info">
          <div className="infoSection">
            <div className="infoSection__svg"><AccessTimeIcon /></div>
            <div className="infoSection__title">ПН-ЧТ 9:00–18:00</div>
          </div>
        </div>
        <div className="info">
          <div className="infoSection">
            <div className="infoSection__svg"><CalendarMonthIcon /></div>
            <div className="infoSection__title">Последняя пятница санитарный день</div>
          </div>
        </div>
      </div>
      <div className="workingMode__section">
        <div className="title">Севастополь, ул. Репина, 3</div>
        <div className="info">
          <div className="infoSection">
            <div className="infoSection__svg"><LocationOnIcon /></div>
            <div className="infoSection__title">Севастополь, ул. Университетская, 29, каб ...</div>
          </div>
        </div>
        <div className="info">
          <div className="infoSection">
            <div className="infoSection__svg"><LocalPhoneIcon /></div>
            <div className="infoSection__title">+7 978 000 00</div>
          </div>
        </div>
        <div className="info">
          <div className="infoSection">
            <div className="infoSection__svg"><AccessTimeIcon /></div>
            <div className="infoSection__title">ПН-ЧТ 9:00–18:00</div>
          </div>
        </div>
        <div className="info">
          <div className="infoSection">
            <div className="infoSection__svg"><CalendarMonthIcon /></div>
            <div className="infoSection__title">Последняя пятница санитарный день</div>
          </div>
        </div>
      </div>

    </>
  );
};
export default Section;
