import Breadcust from "../Common/Breadcust";
import Section from "./components/Section";
import "./WorkingMode.css"

const WorkingMode = (props) => {

  return (
    <div className="workingMode">
      <Breadcust breadcustInfo={['О библиотеке', 'Режим работы']} />
      <div className="wrappped">
        <div className="workingMode__title">Режим работы</div>
        <Section />
      </div>
    </div>
  );
};
export default WorkingMode;
