import LocalizedStrings from 'react-localization';

export let HomeLocalization = new LocalizedStrings({
  rus: {
    title: "Информация об библиотеки:",
    homeRightInfo1: "Библиотека СевГУ (ул. Университетская, 29) одна из крупнейших вузовских библиотек Крыма. Организована в I960 году, на площади в 56 кв. м, с фондом в 10 тысяч экземпляров, полученным в наследство от филиала Николаевского кораблестроительного института, штат 4 человека. В первые годы после открытия учебная литература активно поступала из разных вузов страны (бывшего Советского Союза). Это МГУ, МЭИ, ЛПИ, ОПИ, КПИ и др.",
    homeRightInfo2: "В библиотеке 6 специализированных читальных залов и 1 универсальный читальный зал в учебном корпусе Университета на ул. Гоголя, всего 400 посадочных мест, 3 абонемента, отдел комплектования и обработки литературы, отдел книгохранения, информационно–библиографический отдел, отдел научно–технической документации, сектор автоматизации, хозяйственный отдел (см. структуру библиотеки).",
  },
  eng: {
    title: "Information about libraries:",
    homeRightInfo1: "The SevSU Library (Universitetskaya St., 29) is one of the largest university libraries in the Crimea. Organized in 1960, on an area of ​​56 sq. m, with a fund of 10 thousand copies, inherited from the branch of the Nikolaev Shipbuilding Institute, staff 4 people. In the first years after the opening, educational literature was actively received from various universities in the country (the former Soviet Union). These are Moscow State University, MPEI, LPI, OPI, KPI, etc",
    homeRightInfo2: "The library has 6 specialized reading rooms and 1 universal reading room in the academic building of the University on the street. Gogol, a total of 400 seats, 3 subscriptions, a department for the acquisition and processing of literature, a department for book storage, an information and bibliographic department, a department for scientific and technical documentation, an automation sector, and an economic department (see the structure of the library).",
  },
});