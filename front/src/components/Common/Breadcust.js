import './Breadcust.css'

const Breadcust = ({ breadcustInfo = [] }) => {
    return (
        <div class="text-sm breadcrumbs">
            <ul>
                {
                    breadcustInfo.map(item => {
                        return <li><a>{item}</a></li>
                    })
                }
            </ul>
        </div>
    )
}

export default Breadcust