import './Login.css'
import sevguLogo from '../../images/sevgu-logo.svg'
import { useForm } from "react-hook-form";
import { createProfile, login } from '../../API/AuthApi';
import { useState } from 'react';

import { useNavigate } from "react-router-dom";

const Login = (props) => {

  let navigate = useNavigate();


  const { register, handleSubmit, watch, formState: { errors } } = useForm();

  const [isLogin, setIslogin] = useState(false)

  const onLogin = async data => {
    let user = await login(data)
    if (user.role === 'ADMIN') navigate("/dashboard");
    if (user.role === 'USER') navigate("/");

  }
  const onRegistr = async data => {
    let user = await createProfile(data)
    if (user.role === 'ADMIN') navigate("/dashboard");
    if (user.role === 'USER') navigate("/");
  }
  const onToggleIsLogin = (bool) => { setIslogin(bool) }

  const onSubmit = data => {
    console.log(data);
    if (isLogin) { onLogin(data) }
    else { onRegistr(data) }
  }

  return (
    <div className="wrappped">
      <div className="login">
        <div className='login__modal bg-base-200'>
          <img className='login__modal__logo' src={sevguLogo} />
          <form onSubmit={handleSubmit(onSubmit)}>
            {isLogin && <div className='login__modal__inputs'>
              <div class="form-control w-full ">
                <label class="label">
                  <span class="label-text">Логин</span>
                </label>
                <input type="text" placeholder="Введите свой логин" {...register("login")} class="input input-bordered w-full" />
              </div>
              <div class="form-control w-full ">
                <label class="label">
                  <span class="label-text">Пароль</span>
                </label>
                <input type="text" placeholder="Введите свой пароль" {...register("password")} class="input input-bordered w-full" />
              </div>
              <div className='changeOptions' onClick={() => onToggleIsLogin(false)}>Зарегистрироваться?</div>
              <div className='btns__group'>
                <button class="btn">Submit</button>
              </div>
            </div>}
            {!isLogin && <div className='login__modal__inputs'>
              <div class="form-control w-full ">
                <label class="label">
                  <span class="label-text">Имя</span>
                </label>
                <input type="text" placeholder="Введите свой имя" {...register("name")} class="input input-bordered w-full" />
              </div>
              <div class="form-control w-full ">
                <label class="label">
                  <span class="label-text">Логин</span>
                </label>
                <input type="text" placeholder="Введите свой логин" {...register("login")} class="input input-bordered w-full" />
              </div>
              <div class="form-control w-full ">
                <label class="label">
                  <span class="label-text">Пароль</span>
                </label>
                <input type="text" placeholder="Введите свой пароль" {...register("password")} class="input input-bordered w-full" />
              </div>
              <div className='changeOptions' onClick={() => onToggleIsLogin(true)}>Авторизироваться?</div>
              <div className='btns__group'>
                <button class="btn">Submit</button>
              </div>
            </div>
            }
          </form>
        </div>
      </div>
    </div >
  )
}

export default Login