const axios = require('axios').default;


export const getProfile = (data) => {
    console.log('data', data);

    axios.get('http://localhost:3100/').then(res => {
        console.log('res', res);
    })
}

// headers: {
//     'authorization': your_token,
//     'Accept' : 'application/json',
//     'Content-Type': 'application/json'
// }

export const createProfile = (data) => {
    const user = localStorage.getItem('user')
    const token = localStorage.getItem('token')

    axios.post('http://localhost:3100/auth/reqistration', data, {
        headers: {
            'user': user
        }
    }).then(res => {
        if (res) {
            console.log('res.data', res.data);
            localStorage.setItem('token', res.data.token)
            localStorage.setItem('user', JSON.stringify(res.data))
            return res.data
        }
    })
}

export const login = (data) => {
    const user = localStorage.getItem('user')
    const token = localStorage.getItem('token')

    return axios.post('http://localhost:3100/auth/login', data, {
        headers: {
            'auth': token,
            'user': user
        }
    }).then(res => {
        if (res) {
            localStorage.setItem('token', res.data.token)
            localStorage.setItem('user', JSON.stringify(res.data))
            return res.data
        }
    })
}

export const session = (token) => {
    return axios.post('http://localhost:3100/auth/session', { token }).then(res => {
        let user = JSON.parse(localStorage.getItem('user'))
        if (user.id === res.data.userID) return true
        return false
    })
}

export const getInfo = () => {
    const user = localStorage.getItem('user')
    const token = localStorage.getItem('token')

    return axios.get('http://localhost:3100/info/getInfo', {
        headers: {
            'auth': token,
            'user': user
        }
    }).then(res => {
        console.log('res', res);
        return res.data
    })
}

export const getNews = () => {
    const user = localStorage.getItem('user')
    const token = localStorage.getItem('token')

    return axios.get('http://localhost:3100/info/getNews', {
        headers: {
            'auth': token,
            'user': user
        }
    }).then(res => {
        console.log('res', res);
        return res.data
    })
}

export const getComments = (id) => {
    const user = localStorage.getItem('user')
    const token = localStorage.getItem('token')

    return axios.post('http://localhost:3100/info/getComments', { id: id }, {
        headers: {
            'auth': token,
            'user': user
        }
    }).then(res => {
        return res.data
    })
}

export const createInfo = (data) => {
    const user = localStorage.getItem('user')
    const token = localStorage.getItem('token')

    return axios.post('http://localhost:3100/auth/createInfo', data, {
        headers: {
            'auth': token,
            'user': user
        }
    }).then(res => {
        return res.data
    })
}

export const createComment = (data) => {
    const user = localStorage.getItem('user')
    const token = localStorage.getItem('token')

    return axios.post('http://localhost:3100/auth/createComment', data, {
        headers: {
            'auth': token,
            'user': user
        }
    }).then(res => {
        return res.data
    })
}

export const createNews = (data) => {
    const user = localStorage.getItem('user')
    const token = localStorage.getItem('token')

    return axios.post('http://localhost:3100/auth/createNews', data, {
        headers: {
            'auth': token,
            'user': user
        }
    }).then(res => {
        return res.data
    })
}