import React, { useEffect, useState } from 'react'
import Navbar from '../components/Navbar/Navbar'
import '../components/Info/Info.css'
import { getInfo } from '../API/AuthApi'


export default function InfoRouter() {

    const [info, setInfo] = useState([])

    useEffect(() => {
        getInfoData()
    }, [])


    const getInfoData = async () => {
        const info = await getInfo()
        setInfo([...info.items])
    }


    const InfoItem = (item) => {
        return <div className='info_item'>
            <div className="card w-96 h bg-neutral text-neutral-content">
                <div className="card-body items-center text-center">
                    <h2 className="card-title">{item.title}</h2>
                    <p>{item.info}</p>
                </div>
            </div>
        </div>
    }

    return (
        <>
            <Navbar />
            <div className='info'>
                {info.map(item => {
                    return InfoItem(item)
                })}
            </div>
        </>

    )
}
