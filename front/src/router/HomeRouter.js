import React, { useEffect } from 'react';
import Navbar from '../components/Navbar/Navbar';
import Footer from '../components/Footer/Footer';
import Home from '../components/Home/Home';

const HomeRouter = (props) => {

  useEffect(() => {
  }, [])

  return (
    <>
      <Navbar setLanguage = {props.setLanguage}/>
      <Home/>
      <Footer />
    </>
  );
};
export default HomeRouter;
