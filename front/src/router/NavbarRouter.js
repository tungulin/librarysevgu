import React, { useEffect } from 'react';
import Navbar from '../components/Navbar/Navbar';
import Footer from '../components/Footer/Footer';
import WorkingMode from '../components/WorkingMode/WorkingMode';

const WorkingModeRouter = (props) => {

  useEffect(() => {
  }, [])

  return (
    <>
      <Navbar />
      <WorkingMode />
      <Footer />
    </>
  );
};
export default WorkingModeRouter;
