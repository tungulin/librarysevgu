import React, { useEffect } from 'react';
import Navbar from '../components/Navbar/Navbar';
import Footer from '../components/Footer/Footer';
import Home from '../components/Home/Home';
import News from '../components/News/News';

const NewsRouter = (props) => {

  useEffect(() => {
  }, [])

  return (
    <>
      <Navbar />
      <News/>
      <Footer />
    </>
  );
};
export default NewsRouter;
