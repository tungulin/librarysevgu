module.exports = {
  content: ['./src/**/*.{html,js}'],
  theme: {
    extend: {},
  },
  plugins: [require("daisyui")],

  daisyui: {
    darkTheme:'',
    themes: [
      {
        light: {
          ...require("daisyui/src/colors/themes")["[data-theme=light]"],
          "primary": "#3b82f6",
          'accent':'#fff',
          'neutral':'#3b82f6'
        },

        dark: {
          ...require("daisyui/src/colors/themes")["[data-theme=dark]"],
          "primary": "#3b82f6",
          "accent":'#181a22',
        },
      },
    ],
  }
}
