const db = require('knex')({
    client: 'mysql',
    connection: {
        host: 'localhost',
        port: 3306,
        user: 'root',
        password: 'password',
        database: 'library'
    },
    searchPath: [
        "public"
    ],
    migrations: {
        tableName: 'migrations'
    },
});


module.exports = {
    db,
};