const { db } = require('./utils/db');
var jwt = require('jsonwebtoken');


class infoController {
  getInfo = async (req, res) => {
    let info = await db("info").select()
    res.status(200).json(({ items: info }))
  }

  getNews = async (req, res) => {
    let info = await db("news").select()
    res.status(200).json(({ items: info }))
  }

  getComments = async (req, res) => {
    let info = await db("comments")
      .select('comments.id', 'comments.info', 'usersAuth.name')
      .from('comments')
      .join('usersAuth', 'usersAuth.id', 'comments.authorId')
      .join('news', 'news.id', 'comments.newsId')
      .where('news.id', req.body.id)

    res.status(200).json(({ items: info }))
  }
}


module.exports = new infoController()