const express = require('express')
const authRouter = require('./authRouter')
const infoRouter = require('./infoRouter')
const PORT = 3100
var app = express()
var cors = require('cors')
const jwt = require("jsonwebtoken");
app.use(cors())
app.use(express.json())
app.use('/auth', authRouter)
app.use('/info', infoRouter)

const start = async () => {
    try {
        app.listen(PORT, () => console.log(`server started on port ${PORT}`))
    } catch (e) {
        console.log('error', e);
    }
}

start()