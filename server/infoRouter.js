const Router = require('express')
const bodyParser = require('body-parser')
const router = new Router()
const controller = require('./infoController')

var jsonParser = bodyParser.json();

router.get('/getInfo', controller.getInfo)
router.get('/getNews', controller.getNews)
router.post('/getComments', controller.getComments)

module.exports = router