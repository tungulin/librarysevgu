const { db } = require('./utils/db');
var jwt = require('jsonwebtoken');
const { checkJWT } = require('./utils/utils');
const controllerInfo = require('./infoController')


class authController {
  async reqistration(req, res) {
    try {
      const { name, login, password } = req.body
      await db("usersAuth").insert({ name, login, password })
      res.status(200).json({ name, login, password })
    } catch (e) {
      res.status(400).json({ message: 'Registration error' })
    }
  }

  async login(req, res) {
    const { login, password } = req.body
    let [user] = await db("usersAuth").where({ login, password })
    if (user?.id) {
      var token = jwt.sign({ userID: user.id }, 'shhhhh');
      res.status(200).json(({ ...user, token }))
    } else {
      res.status(400).json({ message: 'Dont have this user!' })
    }
  }

  async session(req, res) {
    const { token } = req.body
    let decoded = jwt.verify(token, 'shhhhh');
    if (decoded) res.status(200).json(decoded)
  }

  async createInfo(req, res) {
    const { title, info } = req.body
    let user = JSON.parse(req.headers.user)
    let token = req.headers.auth

    if (checkJWT(token) && user.role === 'ADMIN') {
      await db("info").insert({ title, info })
      res.status(200).json({ title, info })
    } else {
      res.status(400).json("Invalid token or no access")
    }
  }

  async createNews(req, res) {
    const { title, info } = req.body
    let user = JSON.parse(req.headers.user)
    let token = req.headers.auth

    if (checkJWT(token) && user.role === 'ADMIN') {
      const date = new Date()
      await db("news").insert({ title, info, date })
      res.status(200).json({ title, info, date })
    } else {
      res.status(400).json("Invalid token or no access")
    }
  }

  async createComment(req, res) {
    const { title, info } = req.body
    let user = JSON.parse(req.headers.user)
    let token = req.headers.auth

    if (checkJWT(token)) {
      await db("comments").insert({ authorId: user.id, newsId: req.body.id, info: req.body.info })

      let commentsInfo = await db("comments")
        .select('comments.id', 'comments.info', 'usersAuth.name')
        .from('comments')
        .join('usersAuth', 'usersAuth.id', 'comments.authorId')
        .join('news', 'news.id', 'comments.newsId')
        .where('news.id', req.body.id)

      res.status(200).json(commentsInfo)

    } else {
      res.status(400).json("Invalid token or no access")
    }
  }

  async getUsers(req, res) {
    try {
      res.json('server work')
    } catch (e) {
      console.log('error', e)
    }
  }
}


module.exports = new authController()