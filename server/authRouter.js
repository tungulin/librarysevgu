const Router = require('express')
const bodyParser = require('body-parser')
const router = new Router()
const controller = require('./authController')

var jsonParser = bodyParser.json();

router.post('/reqistration', jsonParser, controller.reqistration)
router.post('/login', controller.login)
router.get('/users', controller.getUsers)
router.post('/session', controller.session)
router.post('/createInfo', controller.createInfo)
router.post('/createNews', controller.createNews)
router.post('/createComment', controller.createComment)

module.exports = router