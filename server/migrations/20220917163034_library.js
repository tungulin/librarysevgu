
exports.up = async function (knex) {
    await knex.schema.createTable("usersAuth", function (table) {
        table.increments("id").primary();
        table.string('name');
        table.string("login").unique();
        table.string("password");
        table.string('role').defaultTo('USER')
    });

    await knex.schema.createTable("info", function (table) {
        table.increments("id");
        table.string('title');
        table.string("info");
    });

    await knex.schema.createTable("news", function (table) {
        table.increments("id").primary();;
        table.string('title');
        table.datetime('date');
        table.string("info");
    });

    await knex.schema.createTable("comments", function (table) {
        table.increments("id");
        table.integer('authorId').unsigned().notNullable();
        table.integer("newsId").unsigned().notNullable();
        table.string("info");

        table.foreign('authorId').references('id').inTable('usersAuth')
        table.foreign('newsId').references('id').inTable('news')
    });
};


exports.down = function (knex) {
    knex.schema.dropTable('usersAuth');
    knex.schema.dropTable('info');
    knex.schema.dropTable('news');
    knex.schema.dropTable('comments');
};
