module.exports = {
    client: 'mysql',
    connection: {
        host: 'localhost',
        port: 3306,
        user: 'root',
        password: 'password',
        database: 'library'
    },
    migrations: {
        tableName: 'migrations'
    },
}